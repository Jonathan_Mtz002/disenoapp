import React, { useState, useEffect } from 'react';
import { View, Text, Button, ActivityIndicator, ScrollView, StyleSheet } from 'react-native';

function HomeScreen() {
  const [todos, setTodos] = useState([]);
  const [loading, setLoading] = useState(true);
  const [displayedInfo, setDisplayedInfo] = useState('');

  useEffect(() => {
    fetch('http://jsonplaceholder.typicode.com/todos')
      .then(response => response.json())
      .then(data => {
        setTodos(data);
        setLoading(false);
      })
      .catch(error => {
        console.error('Hubo un error al obtener los datos:', error);
        setLoading(false);
      });
  }, []);

  // Función para filtrar los pendientes resueltos
  const filterCompletedTodos = () => {
    return todos.filter(todo => todo.completed);
  };

  // Función para filtrar los pendientes sin resolver
  const filterUncompletedTodos = () => {
    return todos.filter(todo => !todo.completed);
  };

  // Función para filtrar los pendientes por usuario
  const filterTodosByUserId = (userId) => {
    return todos.filter(todo => todo.userId === userId);
  };

  const displayInfo = (info) => {
    setDisplayedInfo(info);
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <View style={styles.container}>
        <Text style={styles.title}>Listas de pendientes:</Text>
        
        <View style={styles.buttonContainer}>
          <Button title="1. Solo IDs" onPress={() => displayInfo(todos.map(todo => todo.id).join(', '))} color="#000000" />
        </View>
        <View style={styles.buttonContainer}>
          <Button title="2. IDs y Títulos" onPress={() => displayInfo(todos.map(todo => `${todo.id} - ${todo.title}`).join('\n'))} color="#000000" />
        </View>
        <View style={styles.buttonContainer}>
          <Button title="3. Sin resolver (ID y Título)" onPress={() => displayInfo(filterUncompletedTodos().map(todo => `${todo.id} - ${todo.title}`).join('\n'))} color="#000000" />
        </View>
        <View style={styles.buttonContainer}>
          <Button title="4. Resueltos (IDs y Título)" onPress={() => displayInfo(filterCompletedTodos().map(todo => `${todo.id} - ${todo.title}`).join('\n'))} color="#000000" />
        </View>
        <View style={styles.buttonContainer}>
          <Button title="5. IDs y userID" onPress={() => displayInfo(todos.map(todo => `${todo.id} - ${todo.userId}`).join('\n'))} color="#000000" />
        </View>
        <View style={styles.buttonContainer}>
          <Button title="6. Resueltos (IDs y userID)" onPress={() => displayInfo(filterCompletedTodos().map(todo => `${todo.id} - ${todo.userId}`).join('\n'))} color="#000000" />
        </View>
        <View style={styles.buttonContainer}>
          <Button title="7. Sin resolver (ID y userID)" onPress={() => displayInfo(filterUncompletedTodos().map(todo => `${todo.id} - ${todo.userId}`).join('\n'))} color="#000000" />
        </View>
        
        <Text style={styles.infoTitle}>Información:</Text>
        <Text style={styles.infoText}>{displayedInfo}</Text>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  title: {
    fontWeight: 'bold',
    marginBottom: 10,
  },
  buttonContainer: {
    width: '100%',
    marginBottom: 10,
  },
  infoTitle: {
    fontWeight: 'bold',
    marginTop: 20,
    textAlign: 'center',
  },
  infoText: {
    marginTop: 10,
  },
});

export default HomeScreen;








//Lista de todos los pendientes (Solo IDs)
//Lista de todos los pendientes (IDs y Tittles)
//Lista de todos los pensientes sin resolver (ID y Title)
//Lista de todos los pendientes resueltos (IDs y Title)
//Lista de todos los pendientes (IDs y userID)
//Lista de todos los pendientes resueltos (IDs y userID)
//Lista de todos los pendientes sin resolver (ID y userID)